#!/bin/bash
rm -rf hero-api/app hero-api/obj
helm uninstall hero-api
docker rmi -f $(docker images -q)
