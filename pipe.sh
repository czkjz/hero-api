#!/bin/bash
if [ -z "$1" ]; then
  echo "Нету тега для докер образа. "
  exit 1
fi
echo "Тег докер образа" $1.
echo "####################################################################"
echo "Сборка приложения"
if dotnet build hero-api -o hero-api/app
  then echo "Приложение собрано"
  else echo "Пошло что-то не так"
  exit 1
fi
echo "####################################################################"
echo "Сборка докер образа"
if docker build ./hero-api/ -t hero-api:$1 -t hero-api:latest 
  then echo "Докер имедж готов"
  else echo "Ошибка сборки докер образа"
  exit 1
fi
echo "####################################################################"
echo "Загрузка образа в миникуб"
if minikube image load hero-api:latest &> /dev/null
  then echo "Образ загружен в миникуб"
  else echo "Что-то пошло не так"
  exit 1
fi
echo "####################################################################"
echo "Установка хелм чатра"
if helm install hero-api hero-api-chart &> /dev/null
  then helm status hero-api
  else echo "Хелм чарт не установлен"
  exit 1
fi
echo "####################################################################"
#sleep 30
#kubectl port-forward services/hero-api-hero-api-chart 8080:5000
