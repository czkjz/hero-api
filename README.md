# Simple RESTful Web Service implemented with .NET6
This is a simple lightweight RESTful web service implemented with .NET6. 
The example web service exposes a number of operations that can be used to handle hero objects stored in memory. 
The web service is self hosted and can run on any Windows, Linux or macOS computer (even a raspberry pi).

## Requirements

[.NET6](https://www.microsoft.com/net/learn/get-started/)

## Getting Started

 ```
 cd hero-api 
 dotnet build -o app

 # Linux or macOS
 dotnet app/dotnet6-simple-hero-api-example.dll
 ```

```
# Запуск миникуба
./minikubestart.sh
```

```
# Запуск пайпа, в котором собирается приложение, билдится докер образ, загружается в миникуб и устанавливается хелм чарт 
./pipe.sh
```
```
# Очистка(удаление приложения, докер образов)
./clean.sh
```

Play around with the running web service with curl or Postman.
```
# Command to add a hero
curl -X PUT http://$(minikube ip):30050/api/Heroes -H 'accept: */*' -H 'Content-Type: application/json' -d '{"name": "Superman", "strength": 228}'
 
# Command to delete a hero
curl -X DELETE http://$(minikube ip):30050/api/Heroes/Batman -H 'accept: */*'

# Command to get a specific a hero
curl -X GET http://$(minikube ip):30050/api/Heroes/Super%20Woman -H 'accept: text/plain'

# Command to get all heroes
curl -X GET http://$(minikube ip):30050/api/Heroes -H 'accept: text/plain'
```
